#!/bin/bash
BASE="/data/opendata"
SCRIPT="${BASE}/usr/canada_nar"
WORK="${BASE}/work/ca_network"
LOG="${BASE}/logs/canada_nar.log"
SQL="${SCRIPT}/sql"
PASS=$(cat ${BASE}/env/env_psql | grep "ca_network|"|sed -n -e 's/ca_network|//p')
TMP="${BASE}/tmp"
export PGPASSWORD="${PASS}"
FILE="full_export_ca_[0-9T]+\.zip"
REMOTE="/outbound/master_data_ca/full_export_ca"
USER=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/usm_network|//p')
HOST=$(cat ${BASE}/env/env_conn | grep "usm_network|"|sed -n -e 's/.*@//p'|sed -n -e 's/,.*//p')

dirfile="${TMP}/ca_nwk_dirfile"
echo "DIR FILE :$dirfile"
# Pull directory listing from the FTP
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true; cd ${REMOTE} ; ls; bye;" -u ${USER} ${HOST} > ${dirfile}


# get latest file
LATEST="$(tail -n 1 ${dirfile} | egrep -o "${FILE}")"

echo "Looking for $FILE"
echo "Latest refresh is $LATEST"

if [[ $LATEST == '' ]]
        then
                echo 'Problems getting latest file'
#               mail -s "*ERROR* Daily Load File" -r "ODS DEV <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< "Refresh Not loaded. Problem getting LATEST file. Last file was $LATEST"

                exit
fi


# compare this to the last downloaded file
if [[ ! -f $BASE"lastfile" ]]
        then
                echo "last File missing, touch another."
                touch "${SCRIPT}/lastfile"
                #chmod 664 $BASE"lastfile"
fi

LAST="$(cat "${SCRIPT}/lastfile" | egrep -o "$FILE")"

echo "Last downloaded file is ${LAST}"

if [[ $LAST == $LATEST ]]
        then
                echo 'No new file detected.  Exiting.'
                #mail -s "*ERROR* Daily Load File" -r "ODS DEV <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< "Refresh Not loaded.  Latest file was already downloaded. Last file was $LAST and i see $LATEST"
                exit
        else
                echo "File is different."
                echo $LATEST > "${SCRIPT}/lastfile"
                echo "Will Download new file $REMOTE$LATEST"
fi
# Download the file

#check if the directory exists 
mkdir -p  ${WORK}


DLPATH="${WORK}/${LATEST}"
echo 'Begin Download'
echo "Will download to $DLPATH"

# get -o is broken somehow?  quick fix to allow saving to current directory
cd ${WORK}
lftp -e "set ssl:verify-certificate false; set ftp:ssl-force true; set ftp:ssl-protect-data true;cd $REMOTE ; get $LATEST $DLPATH; bye;" -u $USER $HOST
cd ${SCRIPT}
if [[ ! -f $DLPATH ]]
        then
                echo "No file was downloaded.   Abort"
#                mail -s "*ERROR* Daily Load File" -r "ODS DEV <do_not_reply@veeva.com>"  eric.davis@veeva.com <<< 'Refresh Not loaded.  A new file was not downloaded.'
                exit
else
        echo 'Done Download'
fi

echo "Unzipping $LATEST file ..."
# chmod 664 $DLPATH
#unzip -o $DLPATH -x manifest customkey.csv -d ${TMP}
unzip -o $DLPATH -x manifest customkey.csv -d ${WORK}
# sure we extracted files

time sed -i 's/\x00//g' "${WORK}/hco.csv"
time sed -i 's/\x00//g' "${WORK}/reference.csv"
time sed -i 's/\x00//g' "${WORK}/hcp.csv"
time sed -i 's/\x00//g' "${WORK}/parenthco.csv"
time sed -i 's/\x00//g' "${WORK}/address.csv"
time sed -i 's/\x00//g' "${WORK}/license.csv"
echo 'Truncate table'

PS="$(time psql -U ca_network_user -d ODS < ${SQL}/pg_trunc_tables.sql)"
echo ${PS}

echo 'HCO'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.hco from '${WORK}/hco.csv' with CSV HEADER')"
echo ${PS}


echo 'reference'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.reference from '${WORK}/reference.csv' with CSV HEADER')"
echo ${PS}

echo 'HCP'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.hcp from '${WORK}/hcp.csv' with CSV HEADER')"
echo ${PS}

echo 'ParentHCO'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.parenthco from '${WORK}/parenthco.csv' with CSV HEADER')"
echo ${PS}

echo 'Address'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.address from '${WORK}/address.csv' with CSV HEADER')"
echo ${PS}

echo 'License'
PS="$(time psql -U ca_network_user -d ODS -c '\copy ca_network.license from '${WORK}/license.csv' with CSV HEADER')"
echo ${PS}

echo 'Candidate table'

PS="$(time psql -U ca_network_user -d ODS < ${SQL}/pg_cand_tables.sql)"
echo ${PS}


# Clean up
rm "${WORK}/address.csv"
rm "${WORK}/parenthco.csv"
rm "${WORK}/reference.csv"
rm "${WORK}/hcp.csv"
rm "${WORK}/hco.csv"
rm "${WORK}/license.csv"

rm "${DLPATH}"
