truncate table ca_network.cand_hco;
truncate table ca_network.cand_hcp;
truncate table ca_network.cand_license;
truncate table ca_network.cand_address;
truncate table ca_network.cand_parenthco;


insert into ca_network.cand_hco
select * from ca_network.hco where candidate_record__v = 'true';

insert into ca_network.cand_hcp
select * from ca_network.hcp where candidate_record__v = 'true';

insert into ca_network.cand_license
select a.* from ca_network.license a
inner join ca_network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into ca_network.cand_license
select a.* from ca_network.license a
inner join ca_network.cand_hco b on a.entity_vid__v = b.vid__v;


insert into ca_network.cand_address
select a.* from ca_network.address a
inner join ca_network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into ca_network.cand_address
select a.* from ca_network.address a
inner join ca_network.cand_hco b on a.entity_vid__v = b.vid__v;

insert into ca_network.cand_parenthco
select a.* from ca_network.parenthco a
inner join ca_network.cand_hcp b on a.entity_vid__v = b.vid__v;

insert into ca_network.cand_parenthco
select a.* from ca_network.parenthco a
inner join ca_network.cand_hco b on a.entity_vid__v = b.vid__v;

DELETE from ca_network.parenthco a
using ca_network.cand_parenthco b
where a.vid__v = b.vid__v;

DELETE from ca_network.license a
USING ca_network.cand_license b
where a.vid__v = b.vid__v;

DELETE from ca_network.address a
USING ca_network.cand_address b
where a.vid__v = b.vid__v;

delete from ca_network.hcp where candidate_record__v = 'true';

delete from ca_network.hco where candidate_record__v = 'true';
